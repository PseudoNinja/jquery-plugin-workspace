/**
 * JQuery Workspace Plugin
 * Creates true web application feel to web page
 * @author Eric Miller - "jquery-ws (at) ericmiller (dot) name"
 * @version 2012.01.23
 */
 
    //[CONSTANTS]
var PATH = '/oldcode101_1/jquery-ws/workspace/',
    CSS_PATH = PATH + 'css/jquery-ws.css',
    BODY_CLASSNAME = 'jquery-ws',
    WORKSPACE = 'ws',
    WORKSPACE_CLASSNAME = WORKSPACE,
    WORKSPACE_CLASS = '.' + WORKSPACE_CLASSNAME,
    PANEL = 'panel',
    PANEL_CLASSNAME = WORKSPACE + '-' + PANEL,
    PANEL_CLASS = '.' + PANEL_CLASSNAME,
    PANEL_ANIMATE_DURATION = 'slow',
    PANEL_ANIMATE_BOUNCE_LENGTH = 60,
    PANELS = PANEL + 's',
    PANELS_CLASSNAME = WORKSPACE + '-' + PANELS,
    PANELS_CLASS = '.' + PANELS_CLASSNAME,
    DOCK = 'dock',
    DOCK_CLASSNAME = WORKSPACE + '-' + DOCK,
    STAGE_PADDING = 100,

    //[LOCALS]
    ws, pnl,
    workspaceIndex = 0,
    dockIndex = 0,
    panelIndex = 0,
    browserWidth = $(window).width(),
    browserHeight = $(window).height(),
    browserCenterX = browserWidth / 2,
    browserCenterY = browserHeight / 2;

(function( $ ) {    
    $.fn.to = function(x, y) {
        x = x || 0;
        y = y || 0;
        this.css('left', x);
        this.css('top', y);
        return;
    };
    
    $.fn.moveTo = function(x, y) {
        x = x || 0;
        y = y || 0;
        $(this).animate({
            left:x,
            top:y   
        }, PANEL_ANIMATE_DURATION);
        return;
    };
    
    // if this.x > x; moving left, bounce back right
    // if this.y > y; moving up, bounce back down
    $.fn.bounceTo = function(x, y) {
        var p = this.position(),
            xS = p.left,
            yS = p.top;
            
            
        x = x || 0;
        y = y || 0;

        xS = x + (p.left > x ? -PANEL_ANIMATE_BOUNCE_LENGTH : PANEL_ANIMATE_BOUNCE_LENGTH);
        yS = y + (p.top > y ? -PANEL_ANIMATE_BOUNCE_LENGTH : PANEL_ANIMATE_BOUNCE_LENGTH);
        
        console.debug(x + ':' + xS + ', ' + y + ':' + yS); //@DEBUG
        
        $(this).animate({
            left:xS,
            top:yS   
        }, PANEL_ANIMATE_DURATION).animate({
            left:x,
            top:y   
        }, PANEL_ANIMATE_DURATION);
        return;
    };
    
    $.fn.bounceToCenter = function() {
        var x = 0, 
        y = 0;

        x = browserCenterX - (this.width() / 2);
        y = browserCenterY - (this.height() / 2);

        $(this).bounceTo(x, y);
        return;
    };
    
  $.fn.moveToCenter = function() {
    var x = 0, 
        y = 0;
    
    x = browserCenterX - (this.width() / 2);
    y = browserCenterY - (this.height() / 2);
     
    $(this).moveTo(x, y);
    return;
  };
  
  $.fn.toStageLeft = function() {
    var x = 0, 
        y = 0;
    
    x = 0 - (this.width() + STAGE_PADDING);
    y = browserCenterY - (this.height() / 2);
    
    $(this).to(x, y);
  };
  
   $.fn.moveToStageLeft = function() {
    var x = 0, 
        y = 0;
    
    x = 0 - ( this.width() + STAGE_PADDING);
    y = browserCenterY - (this.height() / 2);
    
    $(this).moveTo(x, y);
  };
  
  $.fn.toStageRight = function() {
    var x = 0, 
        y = 0;
    
    x = (browserWidth + STAGE_PADDING);
    y = browserCenterY - (this.height() / 2);
    
    $(this).to(x, y);
  };
  
  $.fn.moveToStageRight = function() {
    var x = 0, 
        y = 0;
    
    x = (browserWidth + STAGE_PADDING);
    y = browserCenterY - (this.height() / 2);
    
    $(this).moveTo(x, y);
  };
  
  
  $.fn.exists = function(){
      return this.length>0;
  };

    //[OBJECTS]
    function Workspace(){
        this.id = WORKSPACE + '_' + workspaceIndex++;
        this.className = WORKSPACE_CLASSNAME;
        
        return $(document.createElement('div'),{
            id: this.id
        }).addClass(this.className);
    }
    
    function Dock(x, y, h, w){
        this.x = x || 0;
        this.y = y || 0;
        this.h = h || $(window).height();
        this.w = w || 60;    
        
        this.id = DOCK + '_' + dockIndex++;
        this.className = DOCK_CLASSNAME;
        
        return $(document.createElement('div'),{
            id: this.id
        }).addClass(this.className);
    }

    function Panel(){
        this.id = PANEL + '_' + panelIndex;
        this.className = PANEL_CLASSNAME;
        
        return  $(document.createElement('div'),{
            id: this.id
            })
            .addClass(this.className)
            .draggable({ 
                containment: 'parent', 
                stack: PANEL_CLASS 
            });
    }
    
    function PanelWrapper() {        
        return $(document.createElement('div')).addClass(PANELS_CLASSNAME);
    }


    if($('body').hasClass(BODY_CLASSNAME)) {
        $('head').append( $('<link rel="stylesheet" type="text/css" />').attr('href', CSS_PATH) );
        
        ws = new Workspace();
        $('body').append(ws); // create workspace
        $(WORKSPACE_CLASS).append(new PanelWrapper()); // create panels
        $(PANEL_CLASS).appendTo(PANELS_CLASS);// move existing panels to panels wrapper
        // take whatever is left and convert to a panel
        pnl = new Panel();
        $('body').children().not('script, ' + WORKSPACE_CLASS).appendTo(pnl);
        pnl.appendTo(PANELS_CLASS);
    }

    $('a, button, input[type=button], input[type=submit]').click(function(){
        // TODO if local target, reload contents of panel with src from next panel; else load in new panel
        
        return false;
    });

})( jQuery );